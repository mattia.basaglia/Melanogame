/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOGAME_UI_TEXTURE_HPP
#define MELANOGAME_UI_TEXTURE_HPP

#include <string>
#include <memory>
#include <map>

namespace melanogame {
namespace ui {

/**
 * \brief RAII object for the low-level texture device.
 */
class TextureData;

/**
 * \brief Reference-counted wrapper for \c TextureData
 */
using TextureDataOwner = std::shared_ptr<TextureData>;
using TextureDataWeak = std::weak_ptr<TextureData>;

/**
 * \brief Handles texture files
 */
class TextureDirectory
{
public:
    /**
     * \brief Opens the image by file name.
     * \returns A pointer to the corresponding texture
     *
     * If the image was already open, it will return the previously open texture
     *
     * \throws TODO on failure
     */
    TextureDataOwner load(const std::string& filename);

    /**
     * \brief Updates the texture data associated with the given file name
     * \pre \p filename must have been loaded before this function is called
     */
    void update(const std::string& filename, const TextureDataOwner& ptr);

    /**
     * \brief Sets a texture for being unloaded.
     *
     * The texture data will be destructed only after all of the shared owners
     * release their pointers.
     * The directory frees up the slot taken by \p filename so load() will
     * actually open the file again, and update() will not work
     *
     * \pre \p filename must have been loaded before this function is called
     */
    void unload(const std::string& filename);

    /**
     * \brief Ensures the texture data associated with the given file name is
     * updated according to the contents of the file.
     *
     * If such texture does not exist, it will be created.
     */
    void reload(const std::string& filename);

private:
    std::map<std::string, TextureDataOwner> textures;
};

class Texture
{
public:
    Texture(const std::string& file);

    int width() const;
    int height() const;

    TextureDataWeak data() const;

private:
    TextureDataOwner data_;
};

} // namespace ui
} // namespace melanogame
#endif // MELANOGAME_UI_TEXTURE_HPP
