/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOGAME_UI_COLOR_HPP
#define MELANOGAME_UI_COLOR_HPP

#include <cstdint>
#include "melanolib/math/math.hpp"

namespace melanogame {
namespace ui {

class Color
{
public:
    using component_type = uint8_t;
    static constexpr component_type component_min() { return 0; }
    static constexpr component_type component_max() { return 255; }

    template<class T>
        static constexpr component_type component_bound(T value)
    {
        return melanolib::math::bound(component_min(), value, component_max());
    }

    constexpr Color() = default;

    constexpr Color(
        component_type red,
        component_type green,
        component_type blue,
        component_type alpha = component_max()
    ):
        r(component_bound(red)),
        g(component_bound(green)),
        b(component_bound(blue)),
        a(component_bound(alpha))
    {}

    constexpr component_type red() const
    {
        return r;
    }

    constexpr component_type green() const
    {
        return g;
    }

    constexpr component_type blue() const
    {
        return b;
    }

    constexpr component_type alpha() const
    {
        return a;
    }

    constexpr void set_red(component_type value)
    {
        r = component_bound(value);
    }

    constexpr void set_green(component_type value)
    {
        g = component_bound(value);
    }

    constexpr void set_blue(component_type value)
    {
        b = component_bound(value);
    }

    constexpr void set_alpha(component_type value)
    {
        a = component_bound(value);
    }


    constexpr bool operator==(const Color& oth)
    {
        return r == oth.r && g == oth.g && b == oth.b && a == oth.a;
    }

    constexpr bool operator!=(const Color& oth)
    {
        return !(*this == oth);
    }

    static constexpr Color hsv_float(float hue, float sat, float val, float alpha=1)
    {
        // Normalize values
        if ( hue < 0 )
            hue = 0;
        else if ( hue > 1 )
            hue = melanolib::math::fractional(hue);
        sat = melanolib::math::bound(0, sat, 1);
        val = melanolib::math::bound(0, val, 1);

        hue *= 6;
        auto chroma = val * sat;

        auto m = val - chroma;

        int h1 = melanolib::math::floor(hue);
        auto f = hue - h1;

        auto n = val - chroma * f;
        auto k = val - chroma * (1 - f);

        val = melanolib::math::denormalize<float>(val, component_min(), component_max());
        m = melanolib::math::denormalize<float>(m, component_min(), component_max());
        n = melanolib::math::denormalize<float>(n, component_min(), component_max());
        k = melanolib::math::denormalize<float>(k, component_min(), component_max());
        alpha = melanolib::math::denormalize<float>(alpha, component_min(), component_max());

        switch (h1)
        {
            case 0: return Color(val, k, m, alpha);
            case 1: return Color(n, val, m, alpha);
            case 2: return Color(m, val, k, alpha);
            case 3: return Color(m, n, val, alpha);
            case 4: return Color(k, m, val, alpha);
            case 5: return Color(val, m, n, alpha);
            case 6: return Color(val, m, k, alpha);
        }
        return Color();
    }

private:
    component_type r = component_min();
    component_type g = component_min();
    component_type b = component_min();
    component_type a = component_min();

};

constexpr Color blend(const Color& a, const Color& b, float ratio = 0.5)
{
    return Color(
        melanolib::math::round(a.red() * (1-ratio) + b.red() * ratio),
        melanolib::math::round(a.green() * (1-ratio) + b.green() * ratio),
        melanolib::math::round(a.blue() * (1-ratio) + b.blue() * ratio),
        melanolib::math::round(a.alpha() * (1-ratio) + b.alpha() * ratio)
    );
}

} // namespace ui
} // namespace melanogame
#endif // MELANOGAME_UI_COLOR_HPP
