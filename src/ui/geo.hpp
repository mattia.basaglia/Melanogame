/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOGAME_UI_GEO_HPP
#define MELANOGAME_UI_GEO_HPP

#include "melanolib/geo/geo.hpp"

namespace melanogame {
namespace ui {

namespace geo_int {
    using Scalar = int;
    using Point = melanolib::geo::Point<Scalar>;
    using Size = melanolib::geo::Size<Scalar>;
    using Line = melanolib::geo::Line<Scalar>;
    using Rectangle = melanolib::geo::Rectangle<Scalar>;
} // geo_int

namespace geo_float {
    using namespace melanolib::geo::geo_float;
} // geo_float

} // namespace ui
} // namespace melanogame
#endif // MELANOGAME_UI_GEO_HPP
