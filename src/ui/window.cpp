/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "window.hpp"
#include "backend/window_data.hpp"

namespace melanogame {
namespace ui {

Window::Window(geo_int::Rectangle rect, const std::string& title, bool fullscreen)
    : data(std::make_unique<backend::WindowData>(rect, title, fullscreen))
{}

Window::Window(geo_int::Size size, const std::string& title, bool fullscreen)
    : data(std::make_unique<backend::WindowData>(size, title, fullscreen))
{}

geo_int::Rectangle Window::rect() const
{
    return { data->pos(), data->size() };
}

geo_int::Point Window::pos() const
{
    return data->pos();
}

geo_int::Size Window::size() const
{
    return data->size();
}

bool Window::fullscreen() const
{
    return data->fullscreen();
}

std::string Window::title() const
{
    return data->title();
}

Window::~Window() = default;

} // namespace ui
} // namespace melanogame
