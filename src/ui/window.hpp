/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOGAME_UI_WINDOW_HPP
#define MELANOGAME_UI_WINDOW_HPP

#include "ui/geo.hpp"
#include "backend/interface.hpp"

namespace melanogame {
namespace ui {

/**
 * \brief A windows on the desktop environment
 */
class Window
{
public:
    /**
     * \brief Creates a window
     */
    Window(geo_int::Rectangle rect,
           const std::string& title = "",
           bool fullscreen = false);

    /**
     * \brief Creates a centered window
     */
    Window(geo_int::Size size,
           const std::string& title = "",
            bool fullscreen = false);

    ~Window();

    geo_int::Size size() const;

    geo_int::Point pos() const;

    geo_int::Rectangle rect() const;

    bool fullscreen() const;

    std::string title() const;

private:
    std::unique_ptr<backend::WindowData> data;
};


} // namespace ui
} // namespace melanogame
#endif // MELANOGAME_UI_WINDOW_HPP
