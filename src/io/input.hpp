/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOGAME_IO_INPUT_HPP
#define MELANOGAME_IO_INPUT_HPP
namespace melanogame {
namespace io {

enum Key{};

enum MouseButton{};

enum Modifier{};

flags Modifiers;

class Input
{
public:
    virtual ~Input(){}

    virtual void start();

    virtual void stop();

    virtual bool key_down(Key key) = 0;
    virtual Point mouse_pos() = 0;
    virtual Modifiers modifiers() = 0;

protected:
    void send_key_press(Key key) const;
    void send_key_release(Key key) const;
    void send_key_char(uint16_t unicode) const;
    void send_position(const Point& pos) const;
    void send_mouse_down(MouseButton button, const Point& pos);
    void send_mouse_up(MouseButton button, const Point& pos);
    void send_focus_in() const;
    void send_focus_out() const;
    void send_resize(const Size& size) const;
};

} // namespace io
} // namespace melanogame
#endif // GAME_IO_INPUT_HPP
