/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOGAME_IO_DATA_HPP
#define MELANOGAME_IO_DATA_HPP

#include "melanolib/utils/singleton.hpp"
#include "melanolib/string/stringutils.hpp"

namespace melanogame {
namespace io {

class File
{
    /// \todo
};

class Path
{
public:
    enum class Type
    {
        Physical
    };

    Path(std::string path, Type type, bool writable)
        : path_(std::move(path)), type_(type), writable_(writable)
    {}

    const std::string& path() const { return path_; }
    Type type() const { return type_; }
    bool writable() const { return writable_; }

    /**
     * \brief Whether a file with the given name exists within the path
     * \param name A relative file name to be searched in the path
     */
    bool has_file(const std::string& name) const;

    File open_read(const std::string& name) const;

    File open_write(const std::string& name) const;

private:
    std::string path_;
    Type type_;
    bool writable_;

};

class PathList
{
public:
    enum SearchFlags
    {
        Virtual  = 0x00, ///< The file or path doesn't have to exist
        Writable = 0x01, ///< The path must exist and be writable
        Readable = 0x02, ///< The file must exist

        NoThrow  = 0x00, ///< Ignore errors
        Throw    = 0x01, ///< If the file is not found and a result is needed, throw an exception
    };

    void add(const Path& path, int priority);

    std::string search(const std::string& file, SearchFlags flags) const;

    bool has_file(const std::string& name, SearchFlags flags) const;


    File open_read(const std::string& name) const;

    File open_write(const std::string& name) const;
};

/**
 * \brief Class representing a version number.
 */
class Version
{
public:
    enum class Match
    {
        Exact, ///< Two versions must be identical to match
        Least, ///< The first version must be equal to or grater than the second
        Less,  ///< The fist version must be less than the second
        Major, ///< The major version (first component) must match (3.1 matches 3.2)
        Minor, ///< Both major and minor version must match (3.2.1 matches 3.2.2)
    };

    Version() {}

    explicit Version(std::string string)
    {
        auto suffix_begin = std::find_if_not(string.begin(), string.end(),
            [](char c) { return c == '.' || std::isdigit(c); });
        suffix = std::string(suffix_begin, string.end());
        string.erase(suffix_begin, string.end());

        auto strings = melanolib::string::char_split(string, '.');
        components.reserve(strings.size());
        for ( const auto& s : strings )
            components.push_back(melanolib::string::to_int(s));
    }

    explicit Version(std::vector<int> components, std::string suffix = "")
        : components(std::move(components)), suffix(std::move(suffix))
    {}

    std::string string() const
    {
        return melanolib::string::implode(".", components) + suffix;
    }

    bool empty() const
    {
        return components.empty() && suffix.empty();
    }

    bool match(const Version& other, Match match_type) const
    {
        switch ( match_type )
        {
            default:
            case Match::Exact:
                return *this == other;
            case Match::Least:
                return *this >= other;
            case Match::Less:
                return *this < other;
            case Match::Major:
                return components.size() >= 1 && other.components.size() >= 1 &&
                       components[0] == other.components[0];
            case Match::Minor:
                return components.size() >= 2 && other.components.size() >= 2 &&
                       components[0] == other.components[0] &&
                       components[1] == other.components[1];
        }
    }

    bool operator< (const Version& other) const
    {
        return components < other.components ||
            (components == other.components && suffix < other.suffix);
    }

    bool operator<= (const Version& other) const
    {
        return components < other.components ||
            (components == other.components && suffix <= other.suffix);
    }

    bool operator> (const Version& other) const
    {
        return components > other.components ||
            (components == other.components && suffix > other.suffix);
    }

    bool operator>= (const Version& other) const
    {
        return components > other.components ||
            (components == other.components && suffix >= other.suffix);
    }

    bool operator== (const Version& other) const
    {
        return components == other.components && suffix == other.suffix;
    }

    bool operator!= (const Version& other) const
    {
        return components != other.components || suffix != other.suffix;
    }

private:

    std::vector<int> components;
    std::string suffix;
};

class Package
{
public:

    const std::string& name() const
    {
        return name_;
    }

    const std::string& license() const
    {
        return license_;
    }

    const std::string& author() const
    {
        return author_;
    }

    const Version& version() const
    {
        return version_;
    }

    const std::string& init_script() const
    {
        return init_script_;
    }

    const std::map<std::string, std::string>& metadata() const
    {
        return metadata_;
    }

    const PathList& path() const
    {
        return path_;
    }

private:
    PathList path_;
    std::string name_;
    std::string license_;
    std::string author_;
    Version version_;
    std::string init_script_;
    std::map<std::string, std::string> metadata_;
};

class FileSystem : public melanolib::Singleton<FileSystem>
{
public:
    PathList& path()
    {
        return path_;
    }

    std::vector<std::string> packages() const;

    Package package(const std::string& name);
    Package package(const std::string& name,
                    const Version& version = {},
                    Version::Match match = Version::Match::Exact);

private:
    FileSystem() = default;
    friend ParentSingleton;

    PathList path_;
};


} // namespace io
} // namespace melanogame
#endif // MELANOGAME_IO_DATA_HPP
