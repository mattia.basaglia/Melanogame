/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#ifndef MELANOGAME_GAME_HAME_HPP
#define MELANOGAME_GAME_HAME_HPP

#include <string>
#include "melanolib/utils/singleton.hpp"
#include "melanolib/utils/c++-compat.hpp"
#include "ui/window.hpp"

namespace melanogame {
namespace game {

class Game : public melanolib::Singleton<Game>
{
public:
    /**
     * \brief Initialized the game from the command line
     */
    void initialize(std::vector< std::__cxx11::string >& args);

    /**
     * \brief Loads a game package
     */
    void load(const std::string& package_path);

    /**
     * \brief Runs the loaded game package
     */
    void run();


    melanolib::Optional<ui::Window>& window()
    {
        return window_;
    }

private:
    friend ParentSingleton;
    Game() = default;

    melanolib::Optional<ui::Window> window_;
};

} // namespace game
} // namespace melanogame
#endif // MELANOGAME_GAME_HAME_HPP
