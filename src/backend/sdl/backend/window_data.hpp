/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOGAME_BACKEND_SDL_WINDOW_DATA_HPP
#define MELANOGAME_BACKEND_SDL_WINDOW_DATA_HPP

#include <SDL2/SDL.h>

#include "melanolib/geo/geo.hpp"
#include "interface.hpp"

namespace melanogame {
namespace backend {

class WindowData
{
protected:
    explicit WindowData(SDL_Window* window)
    {
        if ( !window )
            throw Error(SDL_GetError());
    }

public:
    WindowData(
        melanolib::geo::geo_int::Rectangle rect,
        const std::string& title = "",
        bool fullscreen = false)
    : WindowData(SDL_CreateWindow(
        title.c_str(),
        rect.x, rect.y,
        rect.width, rect.height,
        SDL_WINDOW_OPENGL | (fullscreen ? SDL_WINDOW_FULLSCREEN : 0)
    ))
    {}

    WindowData(
        melanolib::geo::geo_int::Size size,
        const std::string& title = "",
        bool fullscreen = false)
    : WindowData(SDL_CreateWindow(
        title.c_str(),
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        size.width, size.height,
        SDL_WINDOW_OPENGL | (fullscreen ? SDL_WINDOW_FULLSCREEN : 0)
    ))
    {}


    ~WindowData()
    {
        SDL_DestroyWindow(window);
    }

    melanolib::geo::geo_int::Size size() const
    {
        melanolib::geo::geo_int::Size sz;
        SDL_GetWindowSize(window, &sz.width, &sz.height);
        return sz;
    }

    melanolib::geo::geo_int::Point pos() const
    {
        melanolib::geo::geo_int::Point pt;
        SDL_GetWindowPosition(window, &pt.x, &pt.y);
        return pt;
    }

    bool fullscreen() const
    {
        return SDL_GetWindowFlags(window) & (SDL_WINDOW_FULLSCREEN | SDL_WINDOW_FULLSCREEN_DESKTOP);
    }

    std::string title() const
    {
        return SDL_GetWindowTitle(window);
    }

private:
    SDL_Window* window;
};


} // namespace backend
} // namespace melanogame
#endif // MELANOGAME_BACKEND_SDL_WINDOW_DATA_HPP
