/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOGAME_BACKEND_SDL_TEXTURE_DATA_HPP
#define MELANOGAME_BACKEND_SDL_TEXTURE_DATA_HPP

#include <SDL2/SDL.h>

#include "interface.hpp"

namespace melanogame {
namespace backend {

class TextureData
{
protected:
    explicit TextureData(SDL_Texture* texture)
    {
        if ( !texture )
            throw Error(SDL_GetError());
    }

public:

    ~TextureData();

    /**
     * \brief Width, in pixels of the texture image
     */
    int width() const;

    /**
     * \brief Height, in pixels of the texture image
     */
    int height() const;

private:
    SDL_Texture* texture;
};


} // namespace backend
} // namespace melanogame
#endif // MELANOGAME_BACKEND_SDL_TEXTURE_DATA_HPP
