/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOGAME_BACKEND_INTERFACE_HPP
#define MELANOGAME_BACKEND_INTERFACE_HPP

#include <stdexcept>

namespace melanogame {
namespace backend {

/**
 * \brief RAII object for the low-level texture device.
 */
class TextureData;

/**
 * \brief RAII object for the low-level window resource.
 */
class WindowData;

/**
 * \brief Global back-end singleton
 */
class Backend;

class Error : public std::runtime_error
{
public:
    using runtime_error::runtime_error;
};

} // namespace backend
} // namespace melanogame
#endif // MELANOGAME_BACKEND_INTERFACE_HPP
