/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2016  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#define BOOST_TEST_MODULE Test_Color
#include <boost/test/unit_test.hpp>
#include "ui/color.hpp"

using namespace melanogame::ui;

BOOST_AUTO_TEST_CASE( test_default_ctor )
{
    Color color;
    BOOST_CHECK_EQUAL(color.red(), 0);
    BOOST_CHECK_EQUAL(color.green(), 0);
    BOOST_CHECK_EQUAL(color.blue(), 0);
    BOOST_CHECK_EQUAL(color.alpha(), 0);
}

BOOST_AUTO_TEST_CASE( test_ctor )
{
    Color color(1, 2, 3);
    BOOST_CHECK_EQUAL(color.red(), 1);
    BOOST_CHECK_EQUAL(color.green(), 2);
    BOOST_CHECK_EQUAL(color.blue(), 3);
    BOOST_CHECK_EQUAL(color.alpha(), 255);

    color = Color(1, 2, 3, 4);
    BOOST_CHECK_EQUAL(color.red(), 1);
    BOOST_CHECK_EQUAL(color.green(), 2);
    BOOST_CHECK_EQUAL(color.blue(), 3);
    BOOST_CHECK_EQUAL(color.alpha(), 4);
}

BOOST_AUTO_TEST_CASE( test_constants )
{
    BOOST_CHECK_EQUAL( sizeof(Color::component_type), sizeof(uint8_t) );
    BOOST_CHECK_EQUAL( Color::component_min(), 0 );
    BOOST_CHECK_EQUAL( Color::component_max(), 255 );

    BOOST_CHECK_EQUAL( Color::component_bound(7), 7 );
    BOOST_CHECK_EQUAL( Color::component_bound(-7), Color::component_min() );
    BOOST_CHECK_EQUAL( Color::component_bound(277), Color::component_max() );
}

BOOST_AUTO_TEST_CASE( test_accessors )
{
    Color color;
    color.set_red(123);
    BOOST_CHECK_EQUAL( color.red(), 123 );
    color.set_green(124);
    BOOST_CHECK_EQUAL( color.green(), 124 );
    color.set_blue(125);
    BOOST_CHECK_EQUAL( color.blue(), 125 );
    color.set_alpha(126);
    BOOST_CHECK_EQUAL( color.alpha(), 126 );
}

BOOST_AUTO_TEST_CASE( test_comparison )
{
    Color a(1, 2, 3, 4);
    Color b = a;
    BOOST_CHECK( a == b );
    BOOST_CHECK( !(a != b) );
    b.set_red(99);
    BOOST_CHECK( a != b );
    BOOST_CHECK( !(a == b) );
    b = a;
    b.set_green(99);
    BOOST_CHECK( a != b );
    BOOST_CHECK( !(a == b) );
    b = a;
    b.set_blue(99);
    BOOST_CHECK( a != b );
    BOOST_CHECK( !(a == b) );
    b = a;
    b.set_alpha(99);
    BOOST_CHECK( a != b );
    BOOST_CHECK( !(a == b) );
}

BOOST_AUTO_TEST_CASE( test_hsv )
{
    BOOST_CHECK( Color::hsv_float(0, 1, 0) == Color(0, 0, 0) );

    BOOST_CHECK( Color::hsv_float(0/6.0, 1, 1) == Color(255,   0,   0) );
    BOOST_CHECK( Color::hsv_float(1/6.0, 1, 1) == Color(255, 255,   0) );
    BOOST_CHECK( Color::hsv_float(2/6.0, 1, 1) == Color(  0, 255,   0) );
    BOOST_CHECK( Color::hsv_float(3/6.0, 1, 1) == Color(  0, 255, 255) );
    BOOST_CHECK( Color::hsv_float(4/6.0, 1, 1) == Color(  0,   0, 255) );
    BOOST_CHECK( Color::hsv_float(5/6.0, 1, 1) == Color(255,   0, 255) );
    BOOST_CHECK( Color::hsv_float(6/6.0, 1, 1) == Color(255,   0,   0) );

    BOOST_CHECK( Color::hsv_float(4/6.0 + 1, 1, 1) == Color(0, 0, 255) );
    BOOST_CHECK( Color::hsv_float(4/6.0 - 1, 1, 1) == Color(255, 0, 0) );
}
