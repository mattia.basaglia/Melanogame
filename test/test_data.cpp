/**
\file

\author Mattia Basaglia

\section License

Copyright (C) 2014  Mattia Basaglia

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
#define BOOST_TEST_MODULE Test_Color
#include <boost/test/unit_test.hpp>
#include "io/data.hpp"

using namespace melanogame::io;

BOOST_AUTO_TEST_CASE( test_version )
{
    BOOST_CHECK_EQUAL( "12.3.4", Version("12.3.4").string() );
    BOOST_CHECK_EQUAL( "", Version("").string() );
    BOOST_CHECK_EQUAL( "", Version().string() );

    BOOST_CHECK( Version().empty() );
    BOOST_CHECK( Version("").empty() );


    BOOST_CHECK( Version("3.1.2b").match(Version("3.1.2b"), Version::Match::Exact) );
    BOOST_CHECK( !Version("3.1.2").match(Version("3.1.2b"), Version::Match::Exact) );

    BOOST_CHECK( Version("3.1.2").match(Version("3.1.0"), Version::Match::Least) );
    BOOST_CHECK( !Version("3.1.2").match(Version("3.1.3"), Version::Match::Least) );

    BOOST_CHECK( Version("3.1.2").match(Version("3.1.4"), Version::Match::Less) );
    BOOST_CHECK( !Version("3.1.2").match(Version("3.1.2"), Version::Match::Less) );
    BOOST_CHECK( !Version("3.1.2").match(Version("3.1.1"), Version::Match::Less) );

    BOOST_CHECK( Version("3.1.2").match(Version("3.5.4"), Version::Match::Major) );
    BOOST_CHECK( !Version("3.1.2").match(Version("2.1.2"), Version::Match::Major) );

    BOOST_CHECK( Version("3.1.2").match(Version("3.1.4"), Version::Match::Minor) );
    BOOST_CHECK( !Version("3.1.2").match(Version("3.2.2"), Version::Match::Minor) );
}

